#include <stdlib.h>
#include <stdio.h>

#define GET_BITS(x, msb, lsb)           (((x) & ((1 << ((msb) + 1)) - 1)) >> (lsb))
#define SET_BITS(x, msb, lsb, value)    (((x) & ~(((1 << ((msb) + 1)) - 1) ^ ((1 << (lsb)) - 1))) | (((value) & ((1 << (1 + (msb) - (lsb))) - 1)) << (lsb)))


int main()
{
	unsigned char original = 0xff;
	unsigned char original1 = 0xff;
	unsigned char original2 = 0xff;
	unsigned char original3 = 0xff;

	unsigned char temp = 0x0;
	unsigned char result = 0x0;
	unsigned char result1 = 0x0;
	unsigned char result2 = 0x0;
	unsigned char result3 = 0x0;
	unsigned char bitops = 0x0;

	temp = original;
	result =  (temp & 0xc0) | ((temp & 0x30) >> 1) | ((temp >> 2) & 0x3);	
	printf("original=%x, result=%x\n", original, result);
	
	temp = original1;
	result1 = (original << 6) | ((temp & 0xc0) >> 3) | ((temp & 0x30) >> 4);	
	printf("original1=%x, result1=%x\n", original1, result1);
	
	temp = original2;
	result2 = ((original1 & 0x0c) << 4) | ((original1 & 0x03) << 3) | ((temp & 0xc0) >> 6);	
	printf("original2=%x, result2=%x\n", original2, result2);

	temp = original3;
	result3 = ((temp & 0x30) << 2) | ((temp & 0x0c) << 1) | (temp & 0x03) ;	
	printf("original3=%x, result3=%x\n", original3, result3);

	return 0;
	

#if 0
	unsigned char data[3];
	unsigned char temp[4];

	unsigned char bitops;
	
	data[0]= *buf;
	buf++;
	data[1] = *buf;
	buf++;
	data[2] = *buf;

	
	/*
	  * data[0]  = D7 D6 D5 D4 D3 D2 D1 D0
	  * temp[0] = 0   0   D7 D6 D5 D4 D3 D2 
	  */	
	temp[0] = data[0] >> 2;
	bitops = GET_BITS(temp[0], 5, 4);
	if(bitops == 0x3)
		SET_BITS(temp[0], 7, 5, 7);
	else 
		SET_BITS(temp[0], 7, 5, (bitops << 2));
	
	bitops = GET_BITS(temp[0], 3, 2);
	if(bitops == 0x3)
		SET_BITS(temp[0], 3, 2, 7);
	else 
		SET_BITS(temp[0], 3, 2, (bitops << 2));
	
		
	/*
	  * data[0]  = D1 D0 0   0   0    0   0   0
	  * data[1]  = 0   0   0   0   D7 D6 D5 D4 
	  * temp[1] = D1 D0 0   0   D7 D6 D5 D4 
	  */
	temp[1] = (data[0] << 6) | (data[1] >> 4);
	/*
	  * data[1]  = D3 D2 D1 D0   0    0   0   0
	  * data[2]  = 0   0   0   0      0    0  D7 D6  
	  * temp[2] = D3 D2 D1 D0   0    0   D7 D6  
	  */
	
	temp[2] = (data[1] << 4) | (data[2] >> 6);

	/*
	  * data[2]  = D5 D4 D3 D2 D1 D0      0    0    
	  * temp[3] = D5 D4 D3 D2 D1 D0      0    0  
	  */
	
	temp[3] =  data[2] << 2;
#endif



}
