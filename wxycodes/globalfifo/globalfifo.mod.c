#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x41086e, "module_layout" },
	{ 0x173b7186, "device_create" },
	{ 0x8fe748ab, "__class_create" },
	{ 0xffc7c184, "__init_waitqueue_head" },
	{ 0x40c497a1, "cdev_add" },
	{ 0xf28b622f, "cdev_init" },
	{ 0x105e2727, "__tracepoint_kmalloc" },
	{ 0x8fde5204, "slab_buffer_size" },
	{ 0xfc822e6e, "kmem_cache_alloc_notrace" },
	{ 0xc288f8ce, "malloc_sizes" },
	{ 0x29537c9e, "alloc_chrdev_region" },
	{ 0x2e60bace, "memcpy" },
	{ 0x2da418b5, "copy_to_user" },
	{ 0x642e54ac, "__wake_up" },
	{ 0x4292364c, "schedule" },
	{ 0x71356fba, "remove_wait_queue" },
	{ 0xf2a644fb, "copy_from_user" },
	{ 0x650fb346, "add_wait_queue" },
	{ 0xffd5a395, "default_wake_function" },
	{ 0x7d0bea0b, "per_cpu__current_task" },
	{ 0x3f1899f1, "up" },
	{ 0xb72397d5, "printk" },
	{ 0xfc4f55f3, "down_interruptible" },
	{ 0x85440935, "class_destroy" },
	{ 0x96e2ed60, "device_destroy" },
	{ 0x7485e15e, "unregister_chrdev_region" },
	{ 0x37a0cba, "kfree" },
	{ 0xbef9fe65, "cdev_del" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "2AB93903B8B0421DF46D840");
