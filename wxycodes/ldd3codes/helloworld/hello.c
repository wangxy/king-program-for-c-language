/*
 * hello.c modules 2012.9.21 by wangxy
 */
#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>

static int hello_init(void)
{
	printk(KERN_ALERT "Hello world, enjoy linux forever");
	return 0;
} 


static void hello_cleanup(void)
{
	printk(KERN_ALERT "I am cleanup the hello world modules");
}

module_init(hello_init);
module_exit(hello_cleanup);

MODULE_AUTHOR("wangxy");
MODULE_DESCRIPTION("a simple hello world modules");
MODULE_LICENSE("GPL");
