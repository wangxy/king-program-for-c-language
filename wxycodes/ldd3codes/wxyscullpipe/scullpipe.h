/*
 * scull04.h head file for scull04.c 
 */

#ifndef _SCULLPIPE_H_
#define _SCULLPIPE_H_

#undef WXYDEBUG
#ifdef WXY_DEBUG
  #ifdef __KERNEL__
    #define WXYDEBUG(fmt, args...) printk( KERN_DEBUG "scull: " fmt, ## args)
  #else
    #define WXYDEBUG(fmt, args...) fprintf(stderr, fmt, ## args)
  #endif
#else
  #define WXYDEBUG(fmt, args...)
#endif

#endif








