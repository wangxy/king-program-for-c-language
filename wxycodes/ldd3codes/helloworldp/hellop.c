/*
 * hellop.c simple modules descript module arguments
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/moduleparam.h>

static char *whom = "wangxy";
static int times = 1;

static int hellop_init(void)
{
	int i;
	for (i = 0; i < times; i++)
		printk(KERN_ALERT " %d hello, %s\n", i,whom);
	return 0;
}

static void hellop_cleanup(void)
{
	printk(KERN_ALERT "I will cleanup the hellop modules");
}

module_init(hellop_init);
module_exit(hellop_cleanup);

module_param(times, int, S_IRUGO); /* S_IRUGO defined in file <linux/stat.h>*/
module_param(whom, charp, S_IRUGO); /*this will lies in /sys/module/hellop/parameters/ */
/* module_param_array(name, type, num, perm); */

MODULE_AUTHOR("wangxy");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("simple modules descript module arguments");
