/*
 * scullwuidtest.c   test scullwuid.ko programm
 */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>    /* this three files for open() */

#include <stdio.h>

#define BUFLEN  1000

int main(int argc, char **argv)
{
	int fd, len;
	char buf[BUFLEN];
	
	fd = open("/dev/scullwuid", O_RDWR);

	if (fd < 0) {
		printf("open /dev/scullwuid failture, errno =%d\n", fd);
		return 1;
	} 
	
	len = read(fd, buf, 500);
	if (len < 0 )
		printf("read /dev/scullwuid failture, errno =%d\n", len);
	else 
		printf("read /dev/scullwuid ok\n");
	
	while (getchar() != 'q') ; /* keep the process running */
	
	printf("close /dev/scullwuid now\n");

	close(fd);
	return 0;
}
	
		
 
