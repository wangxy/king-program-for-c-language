/*
 * scullsingle test programm    
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>    /* this three files for open() */

#include <stdio.h>

int main(int argc, char **argv)
{
	int fd1;
	fd1 = open("/dev/scullsingle", O_RDWR);
	
	if (fd1 < 0) {
		printf("open /dev/scullsingle bad\n");
	} else 
		printf("open /dev/scullsingle ok\n");
	
	while (getchar() != 'q') ; /* keep the process running */
	
	printf("close /dev/scullsingle now\n");
	
	close(fd1);
	return 0;
}

