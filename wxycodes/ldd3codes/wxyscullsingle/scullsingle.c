/*
 * scullsingle.c  only permit one process access the device
 */ 

#include <linux/module.h>
#include <linux/init.h>

#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/cdev.h>

#include <asm/uaccess.h>
#include <asm/atomic.h>

#include "scullsingle.h"

static int scullsingle_major = 0;
static dev_t devno;
static struct scullsingle_dev *singledev;

int scull_trim(struct scullsingle_dev *dev)
{
	struct scull_qset *next, *dptr;
	int qset = dev->qset;   /* "dev" is not-null */
	int i;

	for (dptr = dev->data; dptr; dptr = next) { /* all the list items */
		if (dptr->data) {
			for (i = 0; i < qset; i++)
				kfree(dptr->data[i]);
			kfree(dptr->data);
			dptr->data = NULL;
		}
		next = dptr->next;
		kfree(dptr);
	}
	dev->size = 0;
	dev->quantum = 4000;
	dev->qset = 1000;
	dev->data = NULL;
	return 0;
}

struct scull_qset *scull_follow(struct scullsingle_dev *dev, int n)
{
	struct scull_qset *qs = dev->data;

        /* Allocate first qset explicitly if need be */
	if (! qs) {
		qs = dev->data = kmalloc(sizeof(struct scull_qset), GFP_KERNEL);
		if (qs == NULL)
			return NULL;  /* Never mind */
		memset(qs, 0, sizeof(struct scull_qset));
	}

	/* Then follow the list */
	while (n--) {
		if (!qs->next) {
			qs->next = kmalloc(sizeof(struct scull_qset), GFP_KERNEL);
			if (qs->next == NULL)
				return NULL;  /* Never mind */
			memset(qs->next, 0, sizeof(struct scull_qset));
		}
		qs = qs->next;
		continue;
	}
	return qs;
}


static int scullsingle_open(struct inode *inode, struct file *filp)
{
	struct scullsingle_dev *dev = singledev; /* device information */

	if (! atomic_dec_and_test (&dev->available)) {
		atomic_inc(&dev->available);
		return -EBUSY; /* already open */
	}

	/* then, everything else is copied from the bare scull device */
	if ( (filp->f_flags & O_ACCMODE) == O_WRONLY)
		scull_trim(dev);
	filp->private_data = dev;
	return 0;          /* success */
}

ssize_t scullsingle_read(struct file *filp, char __user *buf, size_t count,
                loff_t *f_pos)
{
	struct scullsingle_dev *dev = filp->private_data; 
	struct scull_qset *dptr;	/* the first listitem */
	int quantum = dev->quantum, qset = dev->qset;
	int itemsize = quantum * qset; /* how many bytes in the listitem */
	int item, s_pos, q_pos, rest;
	ssize_t retval = 0;

	if (down_interruptible(&dev->sem))
		return -ERESTARTSYS;
	if (*f_pos >= dev->size)
		goto out;
	if (*f_pos + count > dev->size)
		count = dev->size - *f_pos;

	/* find listitem, qset index, and offset in the quantum */
	item = (long)*f_pos / itemsize;
	rest = (long)*f_pos % itemsize;
	s_pos = rest / quantum; q_pos = rest % quantum;

	/* follow the list up to the right position (defined elsewhere) */
	dptr = scull_follow(dev, item);

	if (dptr == NULL || !dptr->data || ! dptr->data[s_pos])
		goto out; /* don't fill holes */

	/* read only up to the end of this quantum */
	if (count > quantum - q_pos)
		count = quantum - q_pos;

	if (copy_to_user(buf, dptr->data[s_pos] + q_pos, count)) {
		retval = -EFAULT;
		goto out;
	}
	*f_pos += count;
	retval = count;

  out:
	up(&dev->sem);
	return retval;
}

ssize_t scullsingle_write(struct file *filp, const char __user *buf, size_t count,
                loff_t *f_pos)
{
	struct scullsingle_dev *dev = filp->private_data;
	struct scull_qset *dptr;
	int quantum = dev->quantum, qset = dev->qset;
	int itemsize = quantum * qset;
	int item, s_pos, q_pos, rest;
	ssize_t retval = -ENOMEM; /* value used in "goto out" statements */

	if (down_interruptible(&dev->sem))
		return -ERESTARTSYS;

	/* find listitem, qset index and offset in the quantum */
	item = (long)*f_pos / itemsize;
	rest = (long)*f_pos % itemsize;
	s_pos = rest / quantum; q_pos = rest % quantum;

	/* follow the list up to the right position */
	dptr = scull_follow(dev, item);
	if (dptr == NULL)
		goto out;
	if (!dptr->data) {
		dptr->data = kmalloc(qset * sizeof(char *), GFP_KERNEL);
		if (!dptr->data)
			goto out;
		memset(dptr->data, 0, qset * sizeof(char *));
	}
	if (!dptr->data[s_pos]) {
		dptr->data[s_pos] = kmalloc(quantum, GFP_KERNEL);
		if (!dptr->data[s_pos])
			goto out;
	}
	/* write only up to the end of this quantum */
	if (count > quantum - q_pos)
		count = quantum - q_pos;

	if (copy_from_user(dptr->data[s_pos]+q_pos, buf, count)) {
		retval = -EFAULT;
		goto out;
	}
	*f_pos += count;
	retval = count;

        /* update the size */
	if (dev->size < *f_pos)
		dev->size = *f_pos;

  out:
	up(&dev->sem);
	return retval;
}


static int scullsingle_release(struct inode *inode, struct file *filp)
{
	atomic_inc(&singledev->available); /* release the device */
	return 0;
}

struct file_operations scullsingle_fops = {
	.owner =	THIS_MODULE,
	.open =       	scullsingle_open,
	.read =       	scullsingle_read,
	.write =      	scullsingle_write,
	.release =    	scullsingle_release,
};


static int scullsingle_init(void)
{
	int result, err;

	if (scullsingle_major) {
                devno = MKDEV(scullsingle_major, 0);
                result = register_chrdev_region(devno, 1, "scullsingle");
        } else {
                result = alloc_chrdev_region(&devno, 0, 1, "scullsingle");
                scullsingle_major = MAJOR(devno);
        }
        if (result < 0) {
                printk(KERN_WARNING "scullsingle: can't get major %d\n", scullsingle_major);
                return result;
        }

	singledev = kmalloc(sizeof(struct scullsingle_dev), GFP_KERNEL);
	if (!singledev) /* if (singledev == NULL) */ {
                unregister_chrdev_region(devno, 1);
                return -ENOMEM;
        }

        memset(singledev, 0, sizeof(struct scullsingle_dev));

	singledev->quantum = 4000;
	singledev->qset = 1000;
	init_MUTEX(&singledev->sem);
	/* singledev->available = ATOMIC_INIT(1); */
	atomic_set(&singledev->available, 1);

	cdev_init(&singledev->cdev, &scullsingle_fops);
	singledev->cdev.owner = THIS_MODULE;
	err = cdev_add(&singledev->cdev, devno, 1);
	if (err) {
		printk(KERN_NOTICE "Error %d adding scullsingle.\n", err);
		kobject_put(&singledev->cdev.kobj);
		goto fail;
	} else
		printk(KERN_NOTICE "scullsingle registered at %x\n", devno);
	
	return 0;
fail:
	kfree(singledev);
	unregister_chrdev_region(devno, 1);
	return err;
}

static void scullsingle_cleanup(void)
{
	cdev_del(&singledev->cdev);
	scull_trim(singledev);
	kfree(singledev);
	unregister_chrdev_region(devno, 1);
}

module_init(scullsingle_init);
module_exit(scullsingle_cleanup);

MODULE_AUTHOR("WXY");
MODULE_LICENSE("GPL");
