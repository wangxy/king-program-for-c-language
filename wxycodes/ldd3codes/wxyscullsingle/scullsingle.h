/*
 * scull04.h head file for scull04.c 
 */

#ifndef _SCULLSINGLE_H_
#define _SCULLSINGLE_H_

#undef WXYDEBUG
#ifdef WXY_DEBUG
  #ifdef __KERNEL__
    #define WXYDEBUG(fmt, args...) printk( KERN_DEBUG "scull: " fmt, ## args)
  #else
    #define WXYDEBUG(fmt, args...) fprintf(stderr, fmt, ## args)
  #endif
#else
  #define WXYDEBUG(fmt, args...)
#endif

struct scull_qset {
	void **data;
	struct scull_qset *next;
};

struct scullsingle_dev {
	struct scull_qset *data;  /* Pointer to first quantum set */
	int quantum;              /* the current quantum size */
	int qset;                 /* the current array size */
	unsigned long size;       /* amount of data stored here */
	unsigned int access_key;  /* used by sculluid and scullpriv */
	struct semaphore sem;     /* mutual exclusion semaphore     */
	atomic_t available;
	struct cdev cdev;	  /* Char device structure		*/
};

#endif








