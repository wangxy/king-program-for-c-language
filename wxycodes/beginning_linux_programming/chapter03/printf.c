#include <stdio.h>

int main()
{
	printf("printf %%\n");
	printf("printf 20 in %%d %%i is %d and %i\n", 20, 20);
	printf("printf 20 in %%10d %%10i is %10d and %10i\n", 20, 20);
	printf("printf 20 in %%-10d %%-10i is %-10d and %-10i\n", 20, 20);
	printf("printf 20 in %%010d %%010i is %010d and %010i\n", 20, 20);
	printf("printf 20 in %%o %%x is %o and %x\n", 20, 20);
	printf("printf 70 in %%c  is %c\n",70);
	printf("printf 70 in %%s  is %s\n","70");
	printf("printf 70 in %%10s  is %10s\n","70");
	printf("printf 70 in %%*s  is %*s\n",10,"70");
	printf("printf 70 in %%-10s  is %-10s\n","70");
	printf("printf 20.12 in %%f  is %f\n",20.12);
	printf("printf 20.12 in %%10.4f  is %10.4f\n",20.12);
	printf("printf 20 in %%e  is %e\n",20);
	printf("printf 20 in %%g  is %g\n",20);
}
