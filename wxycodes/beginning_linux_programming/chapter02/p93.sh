#!/bin/sh

# $$ ---> process id
trap 'rm -f /tmp/my_tmp_file_$$' INT

echo creating file /tmp/my_tmp_file_$$ 
date > /tmp/my_tmp_file_$$
echo "press interrupt (CTRL-C) to interrupt ...."

while [ -f /tmp/my_tmp_file_$$ ]; do
	echo file exists
	sleep 1
done

echo the file no longer exists

#:<<!EOF!
trap 'rm -f /tmp/my_tmp_file_$$' INT

echo creating file /tmp/my_tmp_file_$$
date > /tmp/my_tmp_file_$$
echo "press interrupt (CTRL-C) to interrupt ...."

while [ -f /tmp/my_tmp_file_$$ ]; do
        echo file exists
        sleep 1
done
#!EOF!

echo we never get here

exit 0
