#!/bin/sh
unset foo
echo ${foo:-bar}
#echo ${foo:=hello}
echo ${foo:?ello}
foo=fud
echo ${foo:-bar}
echo ${foo:=hello}
echo ${foo:+hellohh}
foo=/usr/bin/X11/startx
echo ${#foo}
# * matches zero or more characters
echo $foo
echo ${foo#*/}
echo $foo
echo ${foo##*/}
bar=/usr/local/etc/local/networks
echo $bar
echo ${bar%local*}
echo $bar
echo ${bar%%local*}
exit 0
