#!/bin/sh
echo "Is it morning? Please answer yes or no"
read timeofday
case "$timeofday" in 
	yes | y | Yes | YES )	
		echo "Good morning $timeofday"
		;;
	[nN]* )	
		echo "Good afternoon $timeofday"
		;;
	*  )	
		echo "Sorry, answer nor right"
		echo "Please answer yes or no"
		exit 1
		;;
esac

exit 0

