#!/bin/bash

#some global variables
menu_choice=""
current_cd=""
title_file="title.cdb"
tracks_file="tracks.cdb"
# $$ is process id
temp_file=/tmp/cdb.$$

#trap CTRL + C interrupt
trap 'rm -f $temp_file' EXIT

#define some functions
get_return(){
	echo -e "Press return \c"
	read x
	return 0
}

get_confirm() {
	echo -e "are you sure? \c"
	while true
	do
		read x
		case "$x" in
		[yY]* )
			return 0;;
		[nN]* )
			echo 
			echo "cancelled"
			return 1;;
		* )
			echo "please enter yes or no";;
		esac
	done
}

set_menu_choice() {
	clear
	echo "Options :-"
	echo
	echo "	a) Add new CD"
	echo "	f) Find CD"
	echo "	c) Count the cds and tracks in the catalog"

	if [ "$cdcatnum" != "" ]; then
		echo "	l) List tracks on $cdtitle"
		echo "	r) Remove $cdtitle"
		echo "	u) Update track information for $cdtitle"
	fi

	echo " q) Quit"
	echo
 	echo -e "Please enter choice then press return \c"
	read menu_choice
	return
}

insert_title() {
	echo $* >> $title_file
	return
}

insert_track() {
	echo $* >> $tracks_file
	return
}

add_record_tracks() {
	echo "enter track information for this CD"
	echo "when no more tracks enter q"
	cdtrack=1
	cdttitle=""
	while [ "$cdttitle" != "q" ]
	do
		echo -e "track $cdtrack, track title? \c"
		read tmp
		#not allow enter commas ,
		cdttitle=${tmp%%,*}  
		if [ "$tmp" != "$cdttitle" ];then
			echo "sorry no commas allowed"
			continue
		fi
		
		if [ -n "$cdttitle" ]; then
			if [ "$cdttitle" != "q" ]; then
				insert_track $cdcatnum,$cdtrack,$cdttitle
			fi
		else
			cdtrack=$((cdtrack-1))
		fi
	
		cdtrack=$((cdtrack+1))
	done
}

add_records() {
	#prompt for initial information

	echo -e "enter catalog name \c"
	read tmp
	cdcatnum=${tmp%%,*}
	
	echo -e "enter title \c"
	read tmp
	cdtitle=${tmp%%,*}
	
	echo -e "enter type \c"
	read tmp
	cdtype=${tmp%%,*}
	
	echo -e "enter artist/composer \c"
	read tmp
	cdac=${tmp%%,*}

#check 
	echo about to add new entry
	echo "$cdcatnum $cdtitle $cdtype $cdac"

	if get_confirm ; then
		insert_title $cdcatnum,$cdtitle,$cdtype,$cdac
		add_record_tracks
	else
		remove_records
	fi

	return
}

find_cd() {
	if [ "$1" = "n" ]; then
		asklist=n
	else
		asklist=y
	fi
	cdcatnum=""
	echo -e "enter a string to search in cd title \c"
	read searchstr
	if [ "$searchstr" = "" ]; then
		return 0
	fi

	grep "$searchstr" $title_file > $temp_file

	set $(wc -l $temp_file)
	linesfound=$1

	case "$linesfount" in
	0)	echo "sorry not found"
		get_return
		return 0
		;;
	1)	;;
	2)	echo "sorry not unique"
		echo "find the following"
		cat $temp_file
		get_return
		return 0
	esac
	
	IFS=","
	read cdcatnum cdtitle cdtype cdac < $temp_file
	IFS=""

	if [ -z "$cdcatnum" ]; then
		echo "sorry, can't catalog files"
		get_return
		return 0
	fi

	echo
	echo Catalog number: $cdcatnum
	echo Title: $cdtitle
	echo Type: $cdtype
	echo Artist/Composer: $cdac
	echo
	get_return

	if [ "$asklist" = "y" ]; then
	echo -e "View tracks for this CD? \c"
	read x
	if [ "$x" = "y" ]; then
		echo
		list_tracks
		echo
	fi
	fi
return 1
}

update_cd() {
	if [ -z "$cdcatnum" ]; then
		echo "you must select a cd first"
		find_cd n
	fi
	if [ -n "$cdcatnum" ]; then
		echo "current tracks are :-"
		list_tracks
		echo
		echo "this will re-enter the tracks for $cdtitle"
		get_confirm && {
			grep -v "v${cdcatnum}," $tracks_file > $temp_file
			mv $temp_file $tracks_file
			echo
			add_record_tracks
		}
	fi
	return
}

count_cds() {
	set $(wc -l $title_file)
	num_titles=$1
	set $(wc -l $tracks_file)
	num_tracks=$1
	echo found $num_titles cds,with a total of $num_tracks tracks
	get_return
	return
}

remove_records() {
	if [ -z "$cdcatnum" ]; then
		echo you must select a cd first
		find_cd n
	fi
	if [ -n "$cdcatnum" ]; then
		echo "you are about to delete $cdtitle"
		get_confirm && {
		grep -v  "^${cdcatnum}," $tracks_file > $temp_file
		mv $temp_file $tracks_file
		cdcatnum=""
		echo entry removed
	}
	get_return
	fi
	return
}

list_tracks() {
	if [ "$cdcatnum" = "" ]; then
		echo no cd selected yet
		return
	else
		grep "^${cdcatnum}," $tracks_file > $temp_file
		num_tracks=$(wc -l $temp_file)
		if [ "$num_tracks" = "0" ]; then
			echo no tracks found for $cdtitle
		else {
			echo
			echo "$cdtitle :-"
			cut -f 2- -d , $temp_file
			echo
		} | ${PAGER:-more}
		fi
	fi
	get_return
	return
}

rm -f $temp_file
if [ ! -f $title_file ]; then
	touch $title_file
fi
if [ ! -f $tracks_file ]; then
	touch $tracks_file
fi

#now the application proper
clear
echo
echo
echo "mini cd manager"
sleep 1

quit=n
while [ "$quit" != "y" ];
do
	set_menu_choice
	case "$menu_choice" in
		a) add_records;;
		r) remove_records;;
		f) find_cd y;;
		u) update_cd;;
		c) count_cds;;
		l) list_tracks;;
		b)
			echo
			more $title_file
			echo
			get_return;;
		q | Q )
			quit=y;;
		*) echo "sorry choice not right";;
	esac
done

#exit
rm -f $temp_file
echo "finished"
exit 0
