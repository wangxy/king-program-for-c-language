#!/bin/sh
# Ask some questions and collect the answer
# high 9 lines and width 18 characters
dialog --title "questions for you" --msgbox "Welcome to my survey" 20 40  

dialog --title "confirm" --yesno "are you willing to take part" 20 40

# if you don't want to take part, exit
if [ $? != 0 ]; then
	dialog --infobox "Thank you anyway" 5 20
	sleep 2
	dialog --clear
	exit 0
fi

# 2>_1.txt stdin redirection to _1.txt file
dialog --title "questions input" --inputbox "Please enter your name" 9 30 2>_1.txt 
Q_NAME=$(cat _1.txt)

dialog --menu "$Q_NAME, what music do you like best?" 15 30 4 1 "classical" 2 "jazz" 3 "country" 4 "other" 2>_1.txt
Q_MUSIC=$(cat _1.txt)

#remove _1.txt
rm -f _1.txt

if [ "$Q_MUSIC" = "1" ]; then
	dialog --title "likes classical music" --msgbox "good choice!" 12 25
elif [ "$Q_MUSIC" = "2" ]; then
	dialog --title "likes jazz music" --msgbox "good choice!" 12 25
elif [ "$Q_MUSIC" = "3" ]; then
        dialog --title "likes country music" --msgbox "good choice!" 12 25
else [ "$Q_MUSIC" = "4" ]
        dialog --title "likes other music" --msgbox "good choice!" 12 25
fi

sleep 2
dialog --clear

exit 0
