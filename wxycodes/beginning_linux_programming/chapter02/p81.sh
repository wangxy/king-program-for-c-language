#!/bin/sh

yes_or_no() {
	echo "Is your name $* ?"
	while true
	do
		echo -n "Enter yes or no: "
		read x
		case "$x" in
			[yY]* )
				return 0;;
			[nN]* )
				return 1;;
			* )     
				echo "Answer yes or no"
		esac
	done
}


echo "Original parameters are $* "
if yes_or_no "$1"
	then
		echo "hello $1, nice name"
else
	echo "never mind"
fi


exit 0

