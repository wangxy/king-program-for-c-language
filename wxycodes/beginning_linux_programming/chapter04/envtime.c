#include <time.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main()
{
	int i;
	time_t the_time;
	time_t the_time1, the_time2;
	double diff;
	struct tm *tm_ptr;
#if 0
	for(i = 1; i <= 10; i++) {
		the_time = time((time_t *)0);
		printf("the time is %ld\n", the_time);
		sleep(2);
	}

	the_time1 = time((time_t *)0);
	sleep(3);
	the_time2 = time((time_t *)0);
	
	diff = difftime(the_time2, the_time1);
	printf("the diff time is %lf\n", diff);	
#endif

	(void) time(&the_time);
	tm_ptr = gmtime(&the_time);

	printf("raw time is %ld\n", the_time);
	printf("gmtime give date: %4d/%02d/%02d\n", tm_ptr->tm_year, tm_ptr->tm_mon+1, tm_ptr->tm_mday);
	printf("gmtime give time: %02d:%02d:%02d\n", tm_ptr->tm_hour, tm_ptr->tm_min, tm_ptr->tm_sec);

	tm_ptr = localtime(&the_time);
	printf("raw time is %ld\n", the_time);
        printf("localtime give date: %4d/%02d/%02d\n", tm_ptr->tm_year, tm_ptr->tm_mon+1, tm_ptr->tm_mday);
        printf("localtime give time: %02d:%02d:%02d\n", tm_ptr->tm_hour, tm_ptr->tm_min, tm_ptr->tm_sec);

	the_time1 = mktime(tm_ptr);	
	if (the_time1 == -1) 
		printf("mktime failed\n");
	else 
		printf("mktime time is %ld\n", the_time1);

	(void)time(&the_time2);
	printf("ctime date is: %s\n", ctime(&the_time2));
	exit(0);
}
