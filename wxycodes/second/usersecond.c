
#include <unistd.h>  //read write 
#include <sys/stat.h> //open
#include <sys/types.h> //open
#include <fcntl.h>    //open

#include <stdlib.h>
#include <stdio.h>



int main()
{
	int fd;
	int counter = 0;
	int old_counter = 0;
	
	fd = open("/dev/second", O_RDONLY);
	if (fd != -1) {
		while (1) {
			read(fd, &counter, sizeof(unsigned int));
			if (counter != old_counter) {
				printf("it has passed %d seconds after open /dev/second\n",counter);
				old_counter = counter;
			}
		}
	}
	else {
		printf("Error in open /dev/second\n");
	}
	
	return 0;
}
