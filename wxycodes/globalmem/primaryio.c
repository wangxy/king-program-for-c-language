/*
 *  simple_write.c by wangxinyong 2011.11.26
 */

#include <unistd.h>  //write read
#include <stdlib.h>
int main()
{
	char buffer[128];
	int numread;

	numread = read(0, buffer, 128);
	if (-1 == numread)
		write(2, "A read error has occurred\n", 26);

	if ((write(1, buffer, numread) != numread))
		write(2, "A write error has occurred\n", 27);

//	if ((write(1, "here is some data\n", 18)) != 18)
//		write(2, "A write error has occurred on file descriptor 1\n", 46);
	exit(0);
}
